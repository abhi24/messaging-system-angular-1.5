var myApp = angular.module('myApp', ['ngRoute']);

myApp.config(function($routeProvider){
    $routeProvider

    .when('/home', {
        templateUrl: 'pages/home.html'
    })

    .when('/profile', {
        templateUrl: 'pages/profile.html',
        controller: 'profileCntrl',
        resolve: ['authService', function(authService){
            return authService.isLoggedIn();
        }]
    })

    .when('/login', {
        templateUrl: 'pages/login.html',
        controller: 'loginCntrl'
    })

    .when('/register', {
        templateUrl: 'pages/register.html',
        controller: 'registerCntrl'
    })

    .when('/message', {
        templateUrl: 'pages/message.html',
        controller: 'messageCntrl',
        resolve: ['authService', function(authService){
            return authService.isLoggedIn();
        }]
    })

    .when('/logout', {
        template: `<h1>The user has been logged out successfully</h1>`,
        controller: 'logoutCntrl',
        resolve: ['authService', function(authService){
            return authService.isLoggedIn();
        }]
    })

    .when('/logout', {
        template: `<h1>The user has been logged out successfully</h1>`,
        controller: 'logoutCntrl',

    })
})


myApp.controller('headerController', function($scope, $rootScope) {
    var myIdentity = JSON.parse(localStorage.getItem('logged')) || [];
    console.log(myIdentity['name']);
    if(myIdentity['name']) {
        console.log('I am here');
        $rootScope.bLogin = true;
    } else {
        $rootScope.bLogin = false;
    }
});

myApp.controller('registerCntrl', function($scope, $rootScope, $location) {
    $scope.user= {};
    $scope.registerUser = function() {
        const obj = {
            name: $scope.user.name,
            email: $scope.user.email,
            username: $scope.user.username,
            password: $scope.user.password,
            phone: $scope.user.phone,
            message: []
        }
        var userFromLocalStorage = localStorage.getItem('users');
        if(userFromLocalStorage == null) {
            var val = JSON.parse(localStorage.getItem("users") || "[]");
            val.push(obj);
            localStorage.setItem("users", JSON.stringify(val));
            $location.path('/login');
        } else {
            var val = JSON.parse(localStorage.getItem("users"));
            val.push(obj);
            localStorage.setItem("users", JSON.stringify(val));
            $location.path('/login');
        }
    }
})

myApp.controller('messageCntrl', function($timeout,$scope, $rootScope){
    $scope.isIndented = true;
    var myIdentity = JSON.parse(localStorage.getItem('logged'));
    var myMessages = JSON.parse(localStorage.getItem('users'));

    // update the important messages
    $scope.messageMarkImp = function(message, index) {
        // console.log(message);
        // console.log(index);
        var tempUser = [];

        for(user of myMessages) {
            if(user.username === myIdentity.name) {
                // console.log('I reached here');
                // console.log(user.message[index].important);
                user.message[index].important = !user.message[index].important;
                // console.log(user.message[index].important);
                tempUser.push(user);
            } else {
                tempUser.push(user);
            }
        }
        localStorage.setItem('users', JSON.stringify(tempUser));
    };
  


    // delete the message
    var myIdentity = JSON.parse(localStorage.getItem('logged'));
    var myMessages = JSON.parse(localStorage.getItem('users'));

    $scope.deleteMessage = function(index) {
        // console.log(index);
        var tempUser = [];
        for(user of myMessages) {
            if(user.username === myIdentity.name) {
                user.message.splice(index,1);
                tempUser.push(user);
            } else {
                tempUser.push(user);
            }
        }
        localStorage.setItem('users', JSON.stringify(tempUser));
    }

    $scope.messageSent = false;
    var myIdentity = JSON.parse(localStorage.getItem('logged'));
    var myMessages = JSON.parse(localStorage.getItem('users'));
    $scope.latestMessages;
    for(peeps of myMessages) {
        if(myIdentity.name === peeps.username){
            $scope.latestMessages = peeps.message;
        }
    }    

    // send the messages
    $scope.messageForm = function() {
        var userDetails = JSON.parse(localStorage.getItem('logged'));
        const obj = {
            from: userDetails.name,
            message: $scope.message.messageBody,
            important: false
        }
        var allUsers = JSON.parse(localStorage.getItem('users'));
        var tempUser = [];
        for(user of allUsers) {
            if(user.username === $scope.message.personTo) {
                user.message.push(obj);
                tempUser.push(user);
            } else {
                tempUser.push(user);
            }
        }
        $scope.messageSent = true;
        localStorage.setItem('users', JSON.stringify(tempUser));
        $timeout(function(){
            $scope.messageSent = false;
        }, 2000); 
    }
    
})

myApp.controller('profileCntrl', function($scope, $rootScope,$timeout){
    $scope.flag = false;
    $scope.GetDetails = function (index) {
        // console.log(index);
        $scope.flag = true;
    }

    var userDetails = JSON.parse(localStorage.getItem('logged'));
    // console.log(userDetails.name);
    $scope.defaultUser = userDetails.name;
    $scope.myProfile= {};
    $scope.successMsg = '';

    $scope.onProfileEdited = function() { 
        var user_obj = {
            name: $scope.myProfile.myName,
            email: $scope.myProfile.myEmail,
            username:  $scope.defaultUser,
            phone: $scope.myProfile.myPhone,
            password: userDetails.password,
            message: []
        } 
        // console.log(user_obj);
        var allUsers = JSON.parse(localStorage.getItem('users'));
        var tempUser = [];


        for(user of allUsers) {
            if(user.username === $scope.defaultUser) {
                for(mess of user.message) {
                    user_obj.message.push(mess);
                }
                tempUser.push(user_obj);
            } else {
                tempUser.push(user);
            }
        }
        localStorage.setItem('users', JSON.stringify(tempUser)); 
        $timeout(function(){
            $scope.successMsg = 'Updates made successfully'
        },500)
        $timeout(function(){
            $scope.successMsg = ''
            $scope.flag = false;
            location.reload();
        },2500)
       
    }

    $scope.data = '';
   
    var allUsers = JSON.parse(localStorage.getItem('users'));
    for(person of allUsers){
        if(userDetails.name === person.username) {
            $scope.data = person;
        } 
    }
})

myApp.controller('logoutCntrl', function($scope, $location, $timeout,$rootScope) {
    localStorage.removeItem('logged');
    $timeout(function(){
        $rootScope.bLogin = false;
        $location.path('/login');
    }, 1500)

})
    
myApp.controller('loginCntrl', function($scope, $rootScope, $location, $timeout) {
    var counter = 0;

    var userFromLocalStorage = localStorage.getItem('logged');
    if(userFromLocalStorage ==  null) {
        $rootScope.father = false;
    } else {
        $rootScope.father = true;
        $location.path('/home');
    }

    $scope.user= {};
    $scope.loginUser = function() {
        const obj = {
            name: $scope.login.loginusername,
            password: $scope.login.loginpassword,
        }
        // console.log(obj)
        var userData = JSON.parse(localStorage.getItem('users'));
        for(person of userData) {        
        if(person.username === obj.name) {
            if(person.password === obj.password) {
                counter = 1;
            }
        }
        else {
                    
            }
        }

        if(counter) {
            var userObj = {
                name: $scope.login.loginusername,
                loggedIn: true,
                password: $scope.login.loginpassword
            }
            var val = localStorage.removeItem('logged');
            localStorage.setItem('logged', JSON.stringify(userObj));

            $timeout(function(){
                $rootScope.bLogin = true;
                $location.path('/profile');
            }, 1200)
            } else {
                // console.log('No user found');
            }
    }
})

myApp.factory('authService', function($http, $q, $location){
    return {
        isLoggedIn: function(){
            var promise = $q.defer();
            var myIdentity = JSON.parse(localStorage.getItem('logged'));
            // console.log('myIdentity', myIdentity);
            if(myIdentity == null) {
                promise.reject();
                $location.path('/login');
            } else {
                promise.resolve('true');
            }
            return promise.promise;
        }
    }
})