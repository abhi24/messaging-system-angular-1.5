# README #

This WebSite is developed using **Angular 1.5**

This repository is a **Blog/Shopping Cart WebSite** where users can
> ### User
> 
> 1.   Send messages
> 2.   check messages
> 3.   create account
>



### Downloading the project

Download the entire project and go to the main folder and open using the live server
Also run nodemon from the parent folder in a separate terminal

## Images
>
   **Main Screen**
>
![Alt text](https://bytebucket.org/abhi24/messaging-system-angular-1.5/raw/96abee9ab7c89e4f2b0709d2a8280b147ce6e26d/pages/images/Home.PNG?token=f78cb4f2f4a6adb8a1b28b924166bea8386ef3c1)

>
  **Registration Page**
>
![Alt text](https://bytebucket.org/abhi24/messaging-system-angular-1.5/raw/96abee9ab7c89e4f2b0709d2a8280b147ce6e26d/pages/images/Register.PNG?token=c2396694d8e9841a0f8be9152616928017b43913)

>
   **User/Profile Page** 
>
![Alt text](https://bytebucket.org/abhi24/messaging-system-angular-1.5/raw/96abee9ab7c89e4f2b0709d2a8280b147ce6e26d/pages/images/UM.PNG?token=716890e678caa0ce4163238c5e10e64c8cd9f171) 

>
   **Message Page**
>
![Alt text](https://bytebucket.org/abhi24/messaging-system-angular-1.5/raw/96abee9ab7c89e4f2b0709d2a8280b147ce6e26d/pages/images/message.PNG?token=301fd238e26129d6887201afff07e3101764fc26) 


## Who do I talk to? ###

> Abhishek Gupta
